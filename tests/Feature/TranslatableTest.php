<?php namespace Cuatrokb\Translatable\Test\Feature;

use Cuatrokb\Translatable\Test\TestCase;
use Cuatrokb\Translatable\Translatable;

class TranslatableTest extends TestCase
{
    /** @test */
    public function package_can_define_used_locales() {
        $translatable = app(Translatable::class);
        $this->assertEquals(collect(['en', 'de', 'fr']), $translatable->getLocales());
    }

}
