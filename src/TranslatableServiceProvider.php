<?php namespace Cuatrokb\Translatable;

use Cuatrokb\Translatable\Facades\Translatable;
use Illuminate\Support\ServiceProvider;
use Cuatrokb\Translatable\Providers\ViewComposerProvider;
use Cuatrokb\Translatable\Providers\TranslatableProvider;

class TranslatableServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../install-stubs/config/translatable.php' => config_path('translatable.php'),
            ], 'config');
        }

        $this->app->register(ViewComposerProvider::class);
        $this->app->register(TranslatableProvider::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../install-stubs/config/translatable.php', 'translatable'
        );

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Translatable', Translatable::class);
    }
}
